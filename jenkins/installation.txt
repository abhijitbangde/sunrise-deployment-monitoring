In short the commands run to setup and install Jenkins with Docker-in-Docker (dind) are as follows -

docker network create jenkins

docker run --name jenkins-dind --detach \
--privileged --network jenkins --network-alias docker \
--env DOCKER_TLS_CERTDIR=/certs \
--volume jenkins-docker-certs:/certs/client \
--volume jenkins_home:/var/jenkins_home \
--restart unless-stopped \
--publish 2376:2376 docker:dind --storage-driver overlay2 --insecure-registry 172.27.56.26:5000

docker run --name jenkins --detach -p 49999:8080 -p 50000:50000 \
--cpus="1.0" --memory="2g" \
--network jenkins --env DOCKER_HOST=tcp://docker:2376 \
--env DOCKER_CERT_PATH=/certs/client --env DOCKER_TLS_VERIFY=1 \
-v jenkins_home:/var/jenkins_home \
-v jenkins-docker-certs:/certs/client:ro \
--restart unless-stopped \
--env JENKINS_OPTS="--prefix=/jenkins" 4oh4/jenkins-docker

Troubleshooting guide (work in progress):
To restart Jenkins
$sudo docker stop jenkins
$sudo docker start jenkins